<?php
  if(@$_POST['submit']){
    $kode_jadwal = $_POST['kode_jadwal'];
    $kode_kelas = $_POST['kelas'];
    $kode_dosen = $_POST['nama_dosen'];
    $kode_ruangan = $_POST['ruangan'];
    $mata_kuliah = $_POST['mata_kuliah'];
    $sks = $_POST['sks'];
    $tanggal = $_POST['thn']."-".$_POST['bln']."-".$_POST['tgl'];
    
    mysqli_query($cn, "INSERT INTO jadwal 
      VALUES('".$kode_jadwal."','".$kode_kelas."',
      '".$kode_dosen."','".$kode_ruangan."',
      '".$mata_kuliah."',".$sks.",'".$tanggal."')");
    
    header('Location:index.php?page=list_jadwal');
    exit();
  }
?>
            <form class="mt-2" method="post" action="index.php?page=input_jadwal">
            <h4 class="mb-3">Input Data Jadwal</h4>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kode Jadwal</label>
                <div class="col-sm-3">
                <input type="text" class="form-control" placeholder="Kode Jadwal" name="kode_jadwal">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kelas</label>
                <div class="col-sm-3">
                <select class="form-control" name="kelas">
                  <option value="0">-- Pilih kelas --</option>
          <?php
                    $qr_kelas = mysqli_query($cn, 'SELECT * FROM kelas');
          while($f_kelas = mysqli_fetch_array($qr_kelas)){
          ?>
                  <option value="<?=$f_kelas['kode_kelas']?>">
                      <?=$f_kelas['kelas'];?>
                    </option>
                  <?php
          }
          ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Tanggal</label>
                <div class="col-sm-3">
                <select class="form-control" name="tgl">
                  <option value="0">-- Pilih Tanggal --</option>
          <?php
                    for($tgl=1;$tgl<=31;$tgl++){
          ?>
                    <option value="<?=$tgl;?>"><?=$tgl;?></option>
          <?php
          }
          ?>
                </select>
                </div>
                <div class="col-sm-3">
                <select class="form-control" name="bln">
                  <option value="0">-- Pilih Bulan --</option>
                    <?php
                    $txt_bln = array('Januari','Februari','Maret','April',
                'Mei','Juni','Juli','Agustus','September',
                'Oktober','November','Desember');
          for($bln=0;$bln<12;$bln++){
          ?>
                    <option value="<?=$bln+1;?>">
                      <?=$txt_bln[$bln];?>
                    </option>
          <?php
          }
          ?>
                </select>
                </div>
                <div class="col-sm">
        <input type="text" class="form-control" placeholder="Tahun" name="thn">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Dosen</label>
                <div class="col-sm-3">
                <select class="form-control" name="nama_dosen">
                  <option value="0">-- Pilih Dosen --</option>
                    <?php
                    $qr_dosen = mysqli_query($cn,'SELECT * FROM dosen');
          while($f_dosen = mysqli_fetch_array($qr_dosen)){
          ?>
                  <option value="<?=$f_dosen['kode_dosen'];?>">
                      <?=$f_dosen['nama'];?>
                    </option>
                    <?php
          }
          ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Ruangan</label>
                <div class="col-sm-3">
                <select class="form-control" name="ruangan">
                  <option value="0">-- Pilih Ruangan --</option>
                    <?php
                    $qr_ruangan=mysqli_query($cn,'SELECT * FROM ruangan');
          while($f_ruangan=mysqli_fetch_array($qr_ruangan)){
          ?>
                    <option value="<?=$f_ruangan['kode_ruangan'];?>">
                      <?=$f_ruangan['nama_ruangan'];?>
                    </option>
                    <?php
          }
          ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Matakuliah</label>
                <div class="col-sm-3">
                <input type="text" class="form-control" placeholder="Matakuliah" name="mata_kuliah">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">SKS</label>
                <div class="col-sm-3">
                <input type="text" class="form-control" placeholder="SKS" name="sks">
                </div>
              </div>
              <input name="submit" class="btn btn-primary" type="submit" value="Submit">
        <input class="btn btn-secondary" type="reset" value="Reset">
            </form>