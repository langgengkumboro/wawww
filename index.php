<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="wrap">
        <div class="header">            
           <center><font color="white"><a href="index.php"><h1>Aplikasi Pengelolaan Jadwal Kuliah</h1></font>
            <font color="white"><p>Politeknik LP3I Jakarta Kampus Jakarta Utara</p></font></center>
        </div>
        <div class="menu">
            <ul>
                <li><a href="#">HOME</a></li>
                <li><a href="#">MENU</a></li>
                           
            </ul>
        </div>
        <div class="badan">         
            <div class="sidebar">
                
                <ul>
                    <li><a href="index.php?page=dosen"><font color="white"> Data Dosen</font></a></li>
                   <br>
                    <li><a href="index.php?page=kelas"><font color="white"> Data Kelas</font></a></li>
                    <br>
                    <li><a href="index.php?page=ruangan"><font color="white"> Data Ruangan</font></a></li>
                    <br>
                    <li><a href="index.php?page=input_jadwal"><font color="white">Jadwal</font></a></li><br>
                    <li><a href="index.php?page=list_jadwal"><font color="white">List Jadwal</font></a></li><br>
                    <br><br><br>
                  <li><a href="index.php?page=laporan"><font color="white"> Laporan</font></a></li><br>
                     
                </ul>
                
            </div>
            <div class="content">
    <?php
                include 'koneksi.php';
                $page = @$_GET['page'];
                if($page == 'list_jadwal'){
                    include 'list_jadwal.php';
                }elseif($page == 'input_jadwal'){
                    include 'input_jadwal.php';
                }elseif($page == 'edit_jadwal'){
                    include 'editjadwal.php';
                }
            ?>

    </div>
   
    </div>
</div>
</body>
</html>