<style type="text/css">
   
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  background: #105469;
  font-family: 'Open Sans', sans-serif;
}
table {
  background: #012B39;
  border-radius: 0.25em;
  border-collapse: collapse;
  margin: 1em;
}
th {
  border-bottom: 1px solid #364043;
  color: #E2B842;
  font-size: 0.85em;
  font-weight: 600;
  padding: 0.5em 1em;
  text-align: left;
}
td {
  color: #fff;
  font-weight: 400;
  padding: 0.65em 1em;
}
.disabled td {
  color: #4F5F64;
}
tbody tr {
  transition: background 0.25s ease;
}
tbody tr:hover {
  background: #014055;
}


</style>
<table width="100%" border="1">
  <h1>Data Ruangan</h1>
  <div><b><a href="index.php?hal=inputruangan">[ Tambah Data ]</a></b></div><br/><br/>
  <thead>
    <tr>
      <a href="index.php?hal=input"><i class="fa fa-user-plus fa-2x"></i></a>
      <td align="center" width="2%"><font color="gold"><b>No</font></td>
      <td align="center"><font color="gold"><b>Kode Ruangan</font></td>
      <td align="center"><font color="gold"><b>Nama Ruangan</font></td>
      <td align="center"><font color="gold"><b>letak</font></td>
      <td align="center" bgcolor="black"><font color="gold"><b>Aksi</font></td>
  </thead>
 <?php
    include 'koneksi.php';
    $qr_ruangan = mysqli_query ($cn,"SELECT * FROM ruangan");
    $no=0;
    while ($dt_ruangan = mysqli_fetch_array($qr_ruangan)) {
        $no++;
  ?>
  <tbody>
   <tr>
      <td align="center"><?=$no;?></td>
      <td align="center"><?=$dt_ruangan['kode_ruangan'];?></td>
      <td align="center"><?=$dt_ruangan['nama_ruangan'];?></td>
      <td align="center"><?=$dt_ruangan['letak'];?></td>
      <td align="center" bgcolor="black">
        <a href="editruangan.php?kode_ruangan=$row[kode_ruangan]"><i class="fa-edit">Edit</i></a> 
        <a href='deleteruangan.php?kode_ruangan=$row[kode_ruangan]'
         onclick="return confirm('Are You Sure Delete Data?');" style="text-decoration:none;"><font color="red"><i class="fa fa-trash-o fa-2x">Delete</i></font></a>
      </td>
     
  </tr>
  <?php

      }

  ?>
      
  </tbody>
</table>